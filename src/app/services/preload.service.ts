import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

// Clase para definir las opciones que vamos a usar en el ejemplo
export class PreloadingOptions {
  constructor(public routePath: string, public preload: boolean = true){}
}



@Injectable({
  providedIn: 'root'
})
export class PreloadService {

  private _subject$ = new Subject<PreloadingOptions>();

  // Observable público del que se podrá leer al suscribirse
  public options$ = this._subject$.asObservable();

  constructor() { }

  // Método encargado de plantear rutas para precargar
  startPreload(routePath: string) {
    const opts = new PreloadingOptions(routePath, true);
    // Emitimos unas nuevas opciones de precarga
    this._subject$.next(opts);
  }



}
