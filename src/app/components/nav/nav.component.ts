import { Component } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { PreloadService } from 'src/app/services/preload.service';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent {

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );

  constructor(private breakpointObserver: BreakpointObserver, private _preloadService: PreloadService) {}

  // Ejemplos para PRECARGAR módulos A TRAVÉS DEL SERVICIO
  cargarTodosLosModulos(){
    this._preloadService.startPreload('*'); // cargar todas las rutas y sus módulos de enrutado
  }

  cargarModulo(ruta: string){
    this._preloadService.startPreload(ruta);
  }



}
