import { Injectable } from '@angular/core';
import { PreloadingStrategy, Route } from '@angular/router';
import { Observable, EMPTY } from 'rxjs';

// avoid typing issues for now
export declare var navigator:any;

@Injectable(
  { providedIn: 'root' }
)
export class NetworkAwarePreloadStrategy implements PreloadingStrategy {

  preload(route: Route, load: () => Observable<any>): Observable<any> {
    return this.hasGoodConnection() ? load() : EMPTY;
  }

  hasGoodConnection(): boolean {
    // Se obtiene la conexión del navegador
    const conn = navigator.connection;

    if (conn) {
      // Se evalua si tiene los datos móviles que capan la conexión
      if (conn.saveData) {
        return false; // si es así, no la precarga, devuelve false
      }
      // una lista de conexiones a evitar
      const avoidTheseConnections = ['slow-2g', '2g' /* , '3g', '4g' */];

      // Se obtiene el tipo de conexión a través de effectiveType
      const effectiveType = conn.effectiveType || '';

      // En caso de que la conexión actual esté incluida dentro de la lista a evitar
      if (avoidTheseConnections.includes(effectiveType)) {
        return false; // devuelve false y no se precarga
      }
    }

    // Si no está en la lista, tampoco tiene configurado la restricción de datos móviles
    // precarga las rutas
    return true;
  }
}
