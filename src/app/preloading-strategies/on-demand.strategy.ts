import { Injectable } from '@angular/core';
import { PreloadingStrategy, Route } from '@angular/router';
import { EMPTY, Observable, of } from 'rxjs';
import { mergeMap } from 'rxjs/operators';
import { PreloadingOptions, PreloadService } from '../services/preload.service';

@Injectable({
  providedIn: 'root',
  deps: [
    PreloadService
  ]
})
export class OnDemanPreloadingdStrategy implements PreloadingStrategy {

  private _preloadOnDemandOptions$: Observable<PreloadingOptions>;

  constructor(private _preloadService: PreloadService) {
    this._preloadOnDemandOptions$ = this._preloadService.options$;
  }


  private _decidirSiCargar(route: Route, preloadOptions: PreloadingOptions): boolean {
    return (
      route.data &&
      route.data['preload'] &&
      [route.path, '*'].includes(preloadOptions.routePath) &&
      preloadOptions.preload // por defecto la hemos puesto como true
    );
  }


  preload(route: Route, load: Function): Observable<any> {

    return this._preloadOnDemandOptions$.pipe(
      mergeMap((preloadOptions: PreloadingOptions) => {
        const precargar: boolean = this._decidirSiCargar(route, preloadOptions);
        console.log(`${precargar ? '' : 'NO'} precargamos la ruta: ${route.path}`)
        return precargar ? load() : EMPTY
      })
    );

  }

}
