import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { NetworkAwarePreloadStrategy } from './preloading-strategies/networkaware.strategy';
import { OnDemanPreloadingdStrategy } from './preloading-strategies/on-demand.strategy';
import { OptInPreloadingStrategy } from './preloading-strategies/opt-in.strategy';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'home'
  },
  {
    path: 'home',
    loadChildren: () => import('./modules/home/home.module').then(m => m.HomeModule),
    data: {
      preload: true
    }
  },
  {
    path: 'profile',
    loadChildren: () => import('./modules/profile/profile.module').then(m => m.ProfileModule),
    data: {
      preload: false
    }
  },
  {
    path: 'settings',
    loadChildren: () => import('./modules/settings/settings.module').then(m => m.SettingsModule),
    data: {
      preload: true
    }
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes,
    {
      // * TODO: Descomentar para ver cómo PRECARGAR TODOS LOS MÓDULOS de rutas AL INICIAR LA APLICACIÓN
      // preloadingStrategy: PreloadAllModules
      // * TODO: Descomentar para ver cómo PRECARGAR MÓDULOS QUE TENGAN "data y preload a true" en su config.
      // preloadingStrategy: OptInPreloadingStrategy
      // * TODO: Descomentar para ver cómo PRECARGAR MÓDULOS DEPENDIENDO DE LA CONEXIÓN A INTERNET
      // preloadingStrategy: NetworkAwarePreloadStrategy
      // * PRECARGAR MÓDULOS BAJO DEMANDA --> A TRAVÉS DE CLICKS EN BOTONES EN LA VISTA del NAV
      preloadingStrategy: OnDemanPreloadingdStrategy
    }

  )],
  exports: [RouterModule]
})
export class AppRoutingModule { }
